package POM_test.testWantsomeShop;

import POM.WantsomeShop.Header;
import POM.WantsomeShop.Home;
import POM.WantsomeShop.Register;
import POM.WantsomeShop.User;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static WantsomeShop.setUp.SetUp.setWebDriverProperty;
import static org.junit.Assert.assertTrue;

public class BuilderTest {
    private static WebDriver driver;

    @Before
    public void setUpChrome() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();

    }
    @Test
    public void testBuilder(){
        driver.get("https://practica.wantsome.ro/shop/?page_id=560");
        User user=new User.Builder()
                .withEmail("test.automation183@gmail.com")
                .withPassword("Sebastian17!")
                .withFirstName("Popescu")
                .withLastName("Ionescu")
                .withPhone("0769999999")
                .build();

       /* driver.navigate().to("https://practica.wantsome.ro/shop/");
        Header header = PageFactory.initElements(driver, Header.class);

        Register registerPage = header.goToRegister();
        registerPage.fillRegisterForm(newUser);

        Home home = registerPage.register();
        assertTrue(home.isProductSliderInPage());*/


    }
}
