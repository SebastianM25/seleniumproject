package POM_test.testWantsomeShop;

import POM.WantsomeShop.Blog;
import POM.WantsomeShop.Header;
import POM.WantsomeShop.Home;
import WantsomeBlog.SeleniumIntro.SeleniumIntegration;
import WantsomeShop.testCasePackage.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static WantsomeShop.setUp.SetUp.setWebDriverProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HomePageTest extends TestCase {
    private static WebDriver driver;

    @Before
    public void setUpChrome() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }
    @Test
    public void checkHomePageTitle() {
        driver.get("https://testare-automata.practica.tech/shop/");
        Header header = PageFactory.initElements(driver, Header.class);
        assertTrue(header.isTitleEqualsTo("Wantsome Shop"));
        assertTrue(header.isMainImageDisplayed());
    }
    @Test
    public void checkBlogButtonFunctionallity() {
        driver.get("https://practica.wantsome.ro/shop/");
        Header header = PageFactory.initElements(driver, Header.class);
        Blog blog = header.goToBlog();
        assertTrue(blog.testBlog("BLOG"));
    }
    @Test
    public void checkHomeButtonFunctionallity() {
        driver.get("https://practica.wantsome.ro/shop/");
        Header header = PageFactory.initElements(driver, Header.class);
        Home home = header.goToHome();
        assertTrue(home.testHome("Place to show all products related to Men"));
    }
}
