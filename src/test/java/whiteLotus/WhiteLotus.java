package whiteLotus;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static WantsomeShop.setUp.SetUp.setWebDriverProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WhiteLotus {
    private static WebDriver driver;

    @Before
    public void setUpChrome() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
        //wait= new WebDriverWait(driver,10);
    }

    @Test
    public void testLogin() throws InterruptedException {
        driver.get("http://whitelotus.cristiancotoi.ro/#/");
        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('Sebi','test.automation183@gmail.com')");
        Thread.sleep(500);
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/#/Calendar");
        // Homework: faceti pe pagina asta ce test vreti voi
    }

    @Test
    public void testCalendar() throws InterruptedException {
        driver.get("http://whitelotus.cristiancotoi.ro/#/");
        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('Sebi','test.automation183@gmail.com')");
        Thread.sleep(500);
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/#/Calendar");
        driver.findElement(By.xpath("//abbr[contains(text(),'31')]")).click();
        driver.findElement(By.xpath("//button[@class='react-calendar__navigation__arrow react-calendar__navigation__next-button']")).click();
        String currentURL = "http://whitelotus.cristiancotoi.ro/#/Calendar";
        assertEquals(currentURL, driver.getCurrentUrl());
    }

    @Test
    public void testCalendarChangeYear() throws InterruptedException {
        driver.get("http://whitelotus.cristiancotoi.ro/#/");
        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('Sebi','test.automation183@gmail.com')");
        Thread.sleep(500);
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/#/Calendar");
        driver.findElement(By.xpath("//abbr[contains(text(),'31')]")).click();
        driver.findElement(By.xpath("//button[@class='react-calendar__navigation__arrow react-calendar__navigation__next2-button']")).click();
        assertTrue(driver.getPageSource().contains("octombrie 2020"));
    }



}
