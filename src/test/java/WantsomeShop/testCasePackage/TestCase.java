package WantsomeShop.testCasePackage;

import com.google.gson.internal.$Gson$Preconditions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static WantsomeShop.setUp.SetUp.setWebDriverProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.Keys.ENTER;


public class TestCase {
    private static WebDriver driver;

    protected WebDriverWait wait;
    @Before
    public void setUpChrome() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
        wait= new WebDriverWait(driver,10);
    }
    @Test
    public void testButtonLogin(){
        driver.get("https://practica.wantsome.ro/shop/?page_id=560");
        driver.findElement(By.xpath("//input[@name='login']")).click();
        driver.navigate().refresh();
        assertTrue(driver.getPageSource().contains("Error: Username is required."));
    }

    @Test
    public void checkMeniu() {

        driver.get("https://practica.wantsome.ro/shop/");
        driver.findElement(By.xpath("//div[@class='category-toggle']")).click();
        String currentTitle = driver.getTitle();
        assertTrue(driver.getPageSource().contains("Category"));
        assertTrue(driver.getPageSource().contains("Women"));
        System.out.println("In this page current title is: " + currentTitle.toUpperCase());
    }
    @Test
    public void checkRegister()  {

        driver.get("https://practica.wantsome.ro/shop/?page_id=560");
        driver.findElement(By.id("reg_email")).sendKeys("gmail@gmail.com");
        driver.findElement(By.id("reg_password")).sendKeys("Sebastian122!@");
        driver.findElement(By.id("registration_field_1")).sendKeys("Sebastian");
        driver.findElement(By.id("registration_field_2")).sendKeys("Marius");
        driver.findElement(By.id("registration_field_3")).sendKeys("076000045");
        driver.findElement(By.name("register")).click();
    }
    @Test
    public void checkLoginWithValidUser(){
        driver.get("https://practica.wantsome.ro/shop/?page_id=560");
        driver.findElement(By.id("username")).sendKeys("gmail@gmail.com");
        driver.findElement(By.id("password")).sendKeys("Sebastian122!@");
        driver.findElement(By.xpath("//input[@name='login']")).click();
        System.out.println("Current URL is:"+driver.getCurrentUrl());
        //assertTrue(driver.getPageSource().contains("Home ⁄ My account"));
        assertTrue(driver.getPageSource().contains("Logout"));
        assertTrue(driver.getPageSource().contains("Edit"));
    }
    @Test
    public void checkLoginWithInvalidUser(){
        driver.get("https://practica.wantsome.ro/shop/?page_id=560");
        driver.navigate().refresh();
        driver.findElement(By.id("username")).sendKeys("invalid@gmail.com");
        driver.findElement(By.id("password")).sendKeys("Sebastian122!@");
        driver.findElement(By.xpath("//input[@name='login']")).click();
        //driver.navigate().refresh();
        assertTrue(driver.getPageSource().contains("A user could not be found with this email address."));
    }
    @Test
    public void checkCartPage(){
        driver.get("https://practica.wantsome.ro/shop/");
        driver.findElement(By.xpath("//a[@class='wcmenucart-contents']//i[@class='fa fa-shopping-cart']"))
                .click();
        assertTrue(driver.getPageSource().contains("Your cart is currently empty."));
        System.out.println("Current URL is: "+driver.getCurrentUrl());
    }
    @Test
    public void checkSearch(){
        driver.get("https://practica.wantsome.ro/shop/");
        driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//div[@class='search-icon']")).click();
        driver.findElement(By.xpath("//div[@class='header-search-box active']//input[@placeholder='Search …']")).sendKeys("gold watch", ENTER);

        System.out.println("Current URL is: "+driver.getCurrentUrl());
        driver.navigate().refresh();
    }
    @Test
    public void checkMessageforweakPassword(){
        driver.get("https://practica.wantsome.ro/shop/?page_id=560");
        driver.findElement(By.id("password")).sendKeys("seb");
        assertTrue(driver.getPageSource().contains("Very weak - Please enter a stronger password."));
    }
    @Test
    public void sortDownList(){
        //driver.get("https://practica.wantsome.ro/shop/");
        driver.navigate().to("https://practica.wantsome.ro/shop/?product_cat=women-collection");
        //driver.findElement(By.xpath("//div[@class='tg-container estore-cat-color_37 collection-right-align']//a[contains(text(),'View all')]"));
        WebElement sortDown= driver.findElement(By.cssSelector("select.orderby"));
        Select sortSelect=new Select(sortDown);
        sortSelect.selectByIndex(2);
        assertTrue(true);
        assertTrue(driver.getCurrentUrl().contains("orderby=rating"));
    }
    @Test
    public void verifyCartElementButton() throws InterruptedException {
        driver.navigate().to("https://practica.wantsome.ro/shop");
        By specialXpath= getProductCartButton("Men’s watch");
        WebElement addToCartButton = driver.findElement(specialXpath);
        scrollToElement(addToCartButton);
        addToCartButton.click();

        WebElement cartTopElement = driver.findElement(By.xpath("//a[@class='wcmenucart-contents']"));
        scrollToElement(cartTopElement);
        Thread.sleep(500);

        new Actions(driver)
                .moveToElement(driver.findElement(By.xpath("//a[@class='wcmenucart-contents']")))
                .build().perform();
        Thread.sleep(500);
        String xpathLocatorViewCard="//div[@class='widget woocommerce widget_shopping_cart']//a[@class='button wc-forward'][contains(text(),'View cart')]";
        WebElement viewCartElement=driver.findElement(By.xpath(xpathLocatorViewCard));
        assertTrue(viewCartElement.isDisplayed());
    }

    public void scrollToElement(WebElement addToCartButton) throws InterruptedException {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", addToCartButton);
        Thread.sleep(500);
    }

    public By getProductCartButton(String product) {
        return By.xpath("//a[contains(text(),'"+product+"')]/parent::*/parent::*/div/a");
    }
    @Test
    public void cookiesAreAwsome(){
        driver.navigate().to("https://practica.wantsome.ro/shop");
        Set<Cookie>someCookie=driver.manage().getCookies();
        driver.manage().addCookie(new Cookie("Wantsome cookies","Yes, please!"));
        assertEquals(2,driver.manage().getCookies().size());
        //driver.manage().deleteAllCookies();

    }
    @Test
    public void test(){
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.get("https://practica.wantsome.ro/shop/");
    }
    @Test
    public void waitTest(){

        driver.get("https://singdeutsch.com/");
        By titleXpath=By.xpath("//h1[@class='site-title h1']");
        wait.until(ExpectedConditions.elementToBeClickable(titleXpath));
        assertEquals(
                "Sing Deutsch".toUpperCase(),
                driver.findElement(titleXpath).getText());

    }
    @Test
    public void testAlert() throws InterruptedException {
        driver.get("http://demo.guru99.com/test/delete_customer.php");
        driver.findElement(By.name("cusid")).sendKeys("53920");
        driver.findElement(By.name("submit")).sendKeys("submit", ENTER);
        Alert alert=driver.switchTo().alert();

        String caputre=driver.switchTo().alert().getText();
        System.out.println("The message is: "+caputre);
        alert.accept();
        Thread.sleep(5000);
    }
    @Test
    public void testCopyright(){
        driver.get("https://practica.wantsome.ro/shop/");
        assertTrue(driver.getPageSource().contains("Wantsome Testing shop ©2018"));
    }
    @Test
    public void testComment(){
        driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        driver.get("https://practica.wantsome.ro/shop/");
        driver.findElement(By.xpath("//span[@class='comments-link']")).click();
        driver.navigate().refresh();
        driver.findElement(By.id("comment")).sendKeys("Comentariu");
        driver.findElement(By.id("author")).sendKeys("I am a tester");
        driver.findElement(By.id("author")).clear();
        driver.findElement(By.id("author")).sendKeys("I am the best tester");
        driver.findElement(By.id("email")).sendKeys("tester@gmail.com");
        driver.findElement(By.id("submit")).click();
    }@Test
    public void testCommentAndClearFields(){
        driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        driver.get("https://practica.wantsome.ro/shop/");
        driver.findElement(By.xpath("//span[@class='comments-link']")).click();
        driver.navigate().refresh();
        driver.findElement(By.id("comment")).sendKeys("Comentariuu");
        driver.findElement(By.id("author")).sendKeys("I am a testerr");
        driver.findElement(By.id("author")).sendKeys("I am the best testerr");
        driver.findElement(By.id("email")).sendKeys("tester1@gmail.com");
        driver.findElement(By.id("submit")).click();
        driver.navigate().refresh();
        driver.findElement(By.id("author")).clear();
        driver.findElement(By.id("email")).clear();
        System.out.println("Current URL is: "+driver.getCurrentUrl());
    }
    @Test
    public void searchClear(){
        driver.get("https://practica.wantsome.ro/shop/");
        driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//div[@class='search-icon']")).click();
        driver.findElement(By.xpath("//div[@class='header-search-box active']//input[@placeholder='Search …']")).
                sendKeys("gold watch", Keys.ENTER);
        driver.navigate().refresh();
        driver.findElement(By.xpath("//div[@class='search-icon']")).click();
        driver.findElement(By.xpath("//div[@class='header-search-box active']//input[@placeholder='Search …']")).clear();
    }

    /*@AfterClass
    public static void teardown() {
        driver.close();
    }*/
}
