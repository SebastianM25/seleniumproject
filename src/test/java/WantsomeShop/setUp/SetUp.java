package WantsomeShop.setUp;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class SetUp {
    public static void setWebDriverProperty(String browser) {
        switch (browser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", getDriversPath()+"chromedriver"+getFileException());
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", getDriversPath()+"geckodriver");
                break;
        }
    }
    private static String getProjectPath(){
        return System.getProperty("user.dir");
    }
    private static String getDriversPath(){
        String driversPath;
        //daca pe masina windows
        if(SystemUtils.IS_OS_WINDOWS_7){
            driversPath="\\src\\test\\resources\\drivers\\windows\\";
        }else {
            driversPath="\\src\\test\\resources\\drivers\\mac";
        }
        return getProjectPath()+driversPath;
    }
    private static String getFileException(){
        if (SystemUtils.IS_OS_WINDOWS_7);{
            return ".exe";
        }
    }
}
