package DataDriven;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class PropertisTest {
    @Test
    public void test() throws IOException {
        File file = getFileFromResources("dev_props.csv");
        FileInputStream propsFile = new FileInputStream(file);

        Properties properties= new Properties();
        properties.load(propsFile);

        String browser=properties.getProperty("browser");
        String addr= properties.getProperty("server_addr");
        String port= properties.getProperty("port");
        port=port==null?"80": port;
        assertEquals("chrome", browser);
        assertEquals("localhost", addr);
        assertEquals("80", port);
    }

    public static File getFileFromResources(String fileName) {
        ClassLoader classLoader = DataDrivenTest.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }
}
