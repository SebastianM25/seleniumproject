package DataDriven;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(DataProviderRunner.class)
public class DataDrivenTest {

    @DataProvider
    public static Object[][] powers() throws IOException {
        List<Object[]> outputList = new ArrayList<>();
        List<String> records = readAllLinesFromResourcesFile("DataDriven/user_db.csv");
        for (String record : records) {
            //---------------->  Crtl+Alt+V sa iti aduca stringul
            String[] params = record.split(",");
            outputList.add(params);
        }
       /* return new Object[][]{
                {0, 1},
                {1, 2},
                {2, 4},
                {3, 8},
                {4, 16}
        };*/
        Object[][] result = new Object[records.size()][];
        outputList.toArray(result);
        return result;
    }

    @UseDataProvider("powers")
    @Test
    public void test(String name, String email, String password) {
        System.out.println(name+" has email"+email+" and pass "+password);
        //assertEquals(b, (int) Math.pow(2, a));
    }

    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = DataDrivenTest.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }
}
