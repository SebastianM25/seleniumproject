package WantsomeBlog.SeleniumIntro;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static WantsomeBlog.SeleniumIntro.Utils.setWebDriverProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SeleniumIntegration {
    private  WebDriver driver;
    //protected WebDriverWait wait;

    @Before
    public  void setUp() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
        //wait= new WebDriverWait(driver,10);
    }
    @Test
    public void pageSourceTest(){
        driver.get("https://practica.wantsome.ro/blog");
        System.out.println("Current url is: "+driver.getCurrentUrl());
        System.out.println("Current title is: "+driver.getTitle());
    }

    @Test
    public void testTitle(){
        driver.get("https://practica.wantsome.ro/blog");
        String title=driver.getTitle();
        String words[]=title.split(" ");
        int numberOfWordsWithMoreThan5Letters=0;
        for (String word:words) {
           if(word.length()>5){
               numberOfWordsWithMoreThan5Letters++;
           }
        }
        assertEquals(5,numberOfWordsWithMoreThan5Letters);
    }
    @Test
    public void testPresent(){
        driver.get("https://practica.wantsome.ro/blog");

        assertTrue(driver.getPageSource().contains("The roof is on fireeee"));
    }
}

