package WantsomeBlog.SeleniumIntro;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static WantsomeBlog.SeleniumIntro.Utils.setWebDriverProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MyFirstTest {
    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
    }

    @BeforeClass
    public static void setUpFirefox() {
        setWebDriverProperty("firefox");
        driver = new FirefoxDriver();
    }

    @Test
    public void openChromeBrowser() throws InterruptedException {
        //System.setProperty("webdriver.chrome.driver", "C:\\Code\\seleniumproject\\src\\test\\resources\\drivers\\windows\\chromedriver.exe");
        driver.get("https://practica.wantsome.ro/blog");
        String pageTile = driver.getTitle();
        System.out.println(pageTile);
        Thread.sleep(5000);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        driver.close();
    }
    @Test
    public void openChromeBrowserWithNavigate() {
        //System.setProperty("webdriver.chrome.driver", "C:\\Code\\seleniumproject\\src\\test\\resources\\drivers\\windows\\chromedriver.exe");
        driver.get("https://practica.wantsome.ro/blog");
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().to("https://www.emag.ro/");
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().forward();
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().back();
    }
    @Test
    public void openFirefoxBrowser() throws InterruptedException {
        // System.setProperty("webdriver.gecko.driver", "C:\\Code\\seleniumproject\\src\\test\\resources\\drivers\\windows\\geckodriver.exe");
        setWebDriverProperty("firefox");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://practica.wantsome.ro/blog");
        String pageTile = driver.getTitle();
        System.out.println(pageTile);
        Thread.sleep(5000);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        driver.close();
    }
    @Test
    public void openHTMLUnit() throws InterruptedException {
        //HTML unit nu are interfata grafica, poate da click pe butoane
        //System.setProperty("webdriver.gecko.driver", "C:\\Code\\seleniumproject\\src\\test\\resources\\drivers\\windows\\geckodriver.exe");
        WebDriver driver = new HtmlUnitDriver();
        driver.get("https://practica.wantsome.ro/blog");
        String pageTile = driver.getTitle();
        System.out.println(pageTile);
        Thread.sleep(5000);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
    }

    @AfterClass
    public static void teadown() {
        driver.close();
    }



}
