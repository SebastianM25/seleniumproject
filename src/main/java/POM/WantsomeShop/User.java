package POM.WantsomeShop;

public class User {


    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phone;

    private User(Builder builder){
        this.email=builder.email;
        this.password=builder.password;
        this.firstName=builder.firstName;
        this.lastName=builder.lastName;
        this.phone=builder.phone;
    }
    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public static class Builder{
        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private String phone;

        public Builder withEmail(String email){
            this.email=email;
            return this;
        }
        public Builder withPassword(String password){
            this.password=password;
            return this;
        }
        public Builder withFirstName(String firstName){
            this.firstName=firstName;
            return this;
        }
        public Builder withLastName(String lastName){
            this.lastName=lastName;
            return this;
        }
        public Builder withPhone(String phone){
            this.phone=phone;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
