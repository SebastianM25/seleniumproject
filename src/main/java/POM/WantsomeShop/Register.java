package POM.WantsomeShop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Register {
    @FindBy(id = "reg_email")
    private WebElement email;

    @FindBy(id = "reg_password")
    private WebElement password;

    @FindBy(id = "registration_field_1")
    private WebElement firstName;

    @FindBy(id = "registration_field_2")
    private WebElement lastName;

    @FindBy(id = "registration_field_3")
    private WebElement phone;

    public void register(User user) {
      email.sendKeys(user.getEmail());
      password.sendKeys(user.getPassword());
      firstName.sendKeys(user.getFirstName());
      lastName.sendKeys(user.getLastName());
      phone.sendKeys(user.getPhone());
    }

}
