package POM.WantsomeShop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Blog {
    @FindBy(xpath = "//div[@class='page-header clearfix']/div/h2")
    private WebElement blogTitle;

    public boolean testBlog(String expected) {
        return blogTitle.getText().equals(expected);
    }
}
