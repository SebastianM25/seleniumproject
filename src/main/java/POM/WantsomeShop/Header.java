package POM.WantsomeShop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header{
    private WebDriver driver;

    @FindBy(id = "site-title")
    private WebElement title;
    @FindBy(xpath = "//a[@class='single_image_with_link']//img")
    private WebElement main;
    @FindBy(xpath = "//a[contains(text(),'Blog')]")
    private WebElement blog;
    @FindBy(xpath = "//a[contains(text(),'Home')]")
    WebElement homeButton;
    @FindBy(xpath = "//a[contains(text(),'Connect')]")
    private WebElement connectButton;
    public Header(WebDriver driver){
        this.driver=driver;
    }
    public boolean isMainImageDisplayed(){
        return main.isDisplayed();
    }

    public boolean isTitleEqualsTo(String expected){
        return title.getText().equals(expected);
    }
    public Blog goToBlog(){
        blog.click();
        return PageFactory.initElements(driver,Blog.class);
    }
    public Home goToHome(){
        homeButton.click();
        return PageFactory.initElements(driver,Home.class);
    }
    public Register goToRegister(){
        connectButton.click();
        return  PageFactory.initElements(driver,Register.class);
    }
}
