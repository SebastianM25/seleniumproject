package POM.WantsomeShop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Home {
    @FindBy(xpath = "//h4[contains(text(),'Place to show all products related to Men')]")
    WebElement homeText;

    public boolean testHome(String expected) {
        return homeText.getText().equals(expected);
    }
}
